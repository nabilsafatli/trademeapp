import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        bootstrap()
        return true
    }
    
    private func initWindow() {
        let frame = UIScreen.main.bounds
        window = UIWindow(frame: frame)
    }
    
    private func bootstrap() {
        initWindow()
        
        if let window = window {
            window.rootViewController = getFirstModule()
        }
        window?.makeKeyAndVisible()
    }
    
    private func getFirstModule() -> UINavigationController {
        let categoriesRouter = CategoriesRouter()
        let view = categoriesRouter.bootstrap()
        let navigationController = UINavigationController(rootViewController: view)
        return navigationController
    }
}
