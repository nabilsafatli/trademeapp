import UIKit

class CategoriesView: UIViewController {
    private var categories: Categories?
    private var presenter: CategoriesPresenterProtocol?
    
    //MARK: - IBOutlets
    @IBOutlet weak var categoriesTableView: UITableView! {
        didSet{
            categoriesTableView.isHidden = true
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        presenter?.viewDidLoad()
    }
    
    //MARK: - Private
    
    private func setupNavigationBar() {
        self.navigationItem.title = "Categories"
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            navigationBar.barTintColor = UIColor.trademeBlue
        }
    }
}

//MARK: - CategoriesViewProtocol
extension CategoriesView: CategoriesViewProtocol {
    
    func load(with categories: Categories) {
        self.categories = categories
        categoriesTableView.reloadData()
        activityIndicator.stopAnimating()
        categoriesTableView.isHidden = false
    }
    
    func showAlert(with message: String) {
        activityIndicator.stopAnimating()
        
        let title = "Oops, there was a slip up!"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Try again", style: .default, handler: { [weak self] action in
            self?.presenter?.reloadData()
        })
        
        alert.addAction(retryAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func set(presenter: CategoriesPresenterProtocol) {
        self.presenter = presenter
    }
}

//MARK: - UITableViewDataSource
extension CategoriesView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let categories = categories else {
            return UITableViewCell()
        }
        
        let category = categories.subcategories[indexPath.row].name
        let cell = UITableViewCell()
        cell.textLabel?.text = category
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.subcategories.count ?? 0
    }
}

//MARK: - Protocol
protocol CategoriesViewProtocol: class {
    func load(with categories: Categories)
    func showAlert(with message: String)
    func set(presenter: CategoriesPresenterProtocol)
}
