struct Category: Codable {
    var name: String
    var number: String
    var path: String
    var hasClassifieds: Bool?
    var canHaveSecondCategory: Bool?
    var canBeSecondCategory: Bool?
    var isLeaf: Bool
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case number = "Number"
        case path = "Path"
        case hasClassifieds = "HasClassifieds"
        case canHaveSecondCategory = "CanHaveSecondCategory"
        case canBeSecondCategory = "CanBeSecondCategory"
        case isLeaf = "IsLeaf"
    }
}
