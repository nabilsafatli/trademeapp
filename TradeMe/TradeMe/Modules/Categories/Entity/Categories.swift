struct Categories: Codable {
    var name: String
    var number: String
    var path: String
    var subcategories: [Category]
    var isLeaf: Bool
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case number = "Number"
        case path = "Path"
        case subcategories = "Subcategories"
        case isLeaf = "IsLeaf"
    }
}
