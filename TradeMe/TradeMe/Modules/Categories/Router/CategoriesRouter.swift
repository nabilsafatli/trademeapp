import UIKit

class CategoriesRouter: ModuleBootstrapProtocol {
    func bootstrap() -> UIViewController {
        let httpClientAdapter = HttpClientAdapter()
        let trademeAPI = TrademeAPI(httpClient: httpClientAdapter)
        let interactor = CategoriesInteractor(trademeAPI: trademeAPI)
        let presenter = CategoriesPresenter(interactor: interactor)
        let view = CategoriesView(nibName: "CategoriesView", bundle: nil)
        view.set(presenter: presenter)
        presenter.set(view: view)
        return view
    }
}
