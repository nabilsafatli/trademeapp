import Foundation

class CategoriesPresenter {
    private weak var view: CategoriesViewProtocol?
    private var interactor: CategoriesInteractorProtocol
    
    init(interactor: CategoriesInteractorProtocol) {
        self.interactor = interactor
    }
    
    private func loadData() {
        interactor.getCategories(completion: { [weak self] result in
            switch result {
            case .success(let categories):
                self?.successCallback(with: categories)
                break
            case .failure(let error):
                self?.failureCallback(with: error)
                break
            }
        })
    }
}

extension CategoriesPresenter: CategoriesPresenterProtocol {    
    func viewDidLoad() {
        loadData()
    }
    
    func reloadData() {
        loadData()
    }
    
    func successCallback(with categories: Categories) {
        self.view?.load(with: categories)
    }
    
    func failureCallback(with error: APIError) {
        self.view?.showAlert(with: error.localizedDescription)
    }
    
    func set(view: CategoriesViewProtocol) {
        self.view = view
    }
}


//MARK: - Protocol
protocol CategoriesPresenterProtocol {
    func reloadData()
    func viewDidLoad()
    func set(view: CategoriesViewProtocol)
}
