import Foundation

class CategoriesInteractor {
    private var trademeAPI: TrademeAPIProtocol
    private let path = "Categories.json"
    
    init(trademeAPI: TrademeAPIProtocol) {
        self.trademeAPI = trademeAPI
    }
}

extension CategoriesInteractor: CategoriesInteractorProtocol {
    func getCategories(completion: @escaping CompletionHandler<Categories>) {
        trademeAPI.get(path, completion: completion)
    }
}

protocol CategoriesInteractorProtocol {
    func getCategories(completion: @escaping CompletionHandler<Categories>)
}
