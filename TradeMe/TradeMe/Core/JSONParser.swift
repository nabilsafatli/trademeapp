import Foundation

struct JSONParser {
    static func parse<T: Codable>(_ type: T.Type, jsonArray: [[String: Any]]) -> [T] {
        guard let data = try? JSONSerialization.data(withJSONObject: jsonArray, options: JSONSerialization.WritingOptions.prettyPrinted) else {
            return []
        }
        guard let array = try? JSONDecoder().decode(Array<T>.self, from: data) else {
            return []
        }
        return array
    }
    
    static func parse<T: Codable>(_ type: T.Type, json: [String: Any]) -> T? {
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            return try JSONDecoder().decode(type, from: data)
            
        } catch {
            print("Error: \(error)")
            return nil
        }
    }
}

