struct TrademeAPI: TrademeAPIProtocol {
    private let baseUrl = "https://api.tmsandbox.co.nz/v1/"
    private var httpClient: HttpClientAdapterProtocol
    
    init(httpClient: HttpClientAdapterProtocol) {
        self.httpClient = httpClient
    }

    func get<T>(_ url: String, completion: @escaping CompletionHandler<T>) {
        let completeUrl = baseUrl + url
        httpClient.get(url: completeUrl, completion: completion)
    }
}

protocol TrademeAPIProtocol {
    func get<T>(_ url: String, completion: @escaping CompletionHandler<T>)
}
