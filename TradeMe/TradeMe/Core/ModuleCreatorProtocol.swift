import UIKit

protocol ModuleBootstrapProtocol {
    func bootstrap() -> UIViewController
}
