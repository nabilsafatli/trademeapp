import Foundation
typealias CompletionHandler<T: Codable> = (Result<T>) -> Void

class HttpClientAdapter: HttpClientAdapterProtocol {
    func get<T>(url: String, completion: @escaping CompletionHandler<T>){
        guard let url = URL(string: url) else { return }
        let urlRequest = URLRequest(url: url)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: urlRequest) {
            [weak self] (data, response, error) in
            guard error == nil, let data = data else {
                self?.execute(completion: completion, with: .failure(.invalidResponse))
                return
            }
            
            do {
                guard let responseData = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any], let result = JSONParser.parse(T.self, json: responseData) else {
                    self?.execute(completion: completion, with: .failure(.unknown))
                    return
                }
                self?.execute(completion: completion, with: .success(result))
            }
            catch {
                self?.execute(completion: completion, with: .failure(.unknown))
                return
            }
        }
        task.resume()
    }
    
    private func execute<T>(completion: @escaping CompletionHandler<T>,with result: Result<T>) {
        DispatchQueue.main.async {
            completion(result)
        }
    }
}

protocol HttpClientAdapterProtocol {
    func get<T>(url: String, completion: @escaping CompletionHandler<T>)
}

enum Result<T: Codable> {
    case success(T)
    case failure(APIError)
}

enum APIError: Error {    
    case invalidResponse
    case unknown
    
    var localizedDescription: String {
        switch self {
        case .invalidResponse: return "Invalid response"
        case .unknown: return "Unknown error"
        }
    }
}
