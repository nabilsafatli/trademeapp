import XCTest

class CategoriesPresenterTests: XCTestCase {
    var categoriesPresenter: CategoriesPresenterProtocol!
    
    override func setUp() {
        super.setUp()
        
        let testExpectation = expectation(description: "presenter viewdidload should ask interactor to get categories")
        let view = CategoriesViewMock()
        let interactorMock = CategoriesInteractorMock(expectation: testExpectation)
        
        categoriesPresenter = CategoriesPresenter(interactor: interactorMock)
        categoriesPresenter.set(view: view)
    }
    
    func testPresenterLoadCallsViewToUpdate() {
        categoriesPresenter.viewDidLoad()
        waitForExpectations()
    }
}

class CategoriesInteractorMock: CategoriesInteractorProtocol {
    private var expectation: XCTestExpectation
    
    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }
    
    func getCategories(completion: @escaping CompletionHandler<Categories>) {
        expectation.fulfill()
    }
}

class CategoriesViewMock: CategoriesViewProtocol {
    func showAlert(with message: String) {}
        
    func load(with categories: Categories) { }
    
    func set(presenter: CategoriesPresenterProtocol) {}
}
