import XCTest

extension XCTestCase {
    func waitForExpectations() {
        self.waitForExpectations(timeout: 1)
    }
}
