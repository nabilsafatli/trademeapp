## Nabil Safatli - TradeMeApp - Coding challenge

# README #

### Synopsis

Hey there, this app will bring you the categories available at Trademe.

The application is using VIPER, which I found to be a more clear way to work with iOS after reading some articles:

- https://medium.com/ios-os-x-development/ios-architecture-patterns-ecba4c38de52
- https://www.objc.io/issues/13-architecture/viper/

I like to follow the git flow on my projects, so you will be able to check everything on the develop or master branches. The last tag is 0.2 on master where I finished the last commit.

### Platform and language chosen

iOS and Swift, because Swift is awesome :)
Swift brings a new way to develop for iOS with powerfull generics and enums that I am really enjoy to use.

## Highlights of the project

- I always try to follow the Swift code style in my iOS apps.
- The use of SOLID principles where each slice of the VIPER module does exaclty its responsability 
- Unit testing is really important, I did some just to show my knowledge about it. Even though I haven't written many tests all the code was developed thinking on making it easy to write my tests later
- You will notice that I always make my dependencies injected through the constructor where that will help me to write unit tests. Unless in the case of the presenter where I have a specific method to set the view.
- I chose not to use any lib on the project because I wanted to show more about my coding style and as it was a really simple request it didn't make sense to bring any huge lib to the app.

### What could have been done in a better way?

Unfortunately this has been a really hard week at my job and I only had time to work in that test on saturday
You will notice that I could'nt implement everything asked and I explained with more details in the mail sent to trademe.
Things that I would like to improve:
- Separate the injector from the router
- Improve the layout of categories to be more friendly to the user
- Maybe use Swinject to take care of my dependencies, but I haven't studied it yet
- Write more tests for each slice of the VIPER module
- Organize the Core better to make it easier for any developer to find the class in the right context

### How to test

Command + U will run some unit tests of the Categories Module.